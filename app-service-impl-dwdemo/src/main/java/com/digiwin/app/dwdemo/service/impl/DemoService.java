package com.digiwin.app.dwdemo.service.impl;

import com.digiwin.app.dwdemo.service.IDemoService;

/**
 * @author Miko
 */
public class DemoService implements IDemoService {
    @Override
    public Object get() throws Exception {

        return "Hello Application.";
    }
}
